    <!-- footer start-->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 footer-copyright">
                    <p class="mb-0">Copyright 2019 © Multikart All rights reserved.</p>
                </div>
                <div class="col-md-6">
                    <p class="pull-right mb-0">Hand crafted &amp; made with<i class="fa fa-heart"></i></p>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end-->
</div>

</div>

<!-- latest jquery-->
<script src="{{ asset("back/assets/js/jquery-3.3.1.min.js") }}"></script>

<!-- Bootstrap js-->
<script src="{{ asset("back/assets/js/popper.min.js") }}"></script>
<script src="{{ asset("back/assets/js/bootstrap.js") }}"></script>

<!-- feather icon js-->
<script src="{{ asset("back/assets/js/icons/feather-icon/feather.min.js") }}"></script>
<script src="{{ asset("back/assets/js/icons/feather-icon/feather-icon.js") }}"></script>

<!-- Sidebar jquery-->
<script src="{{ asset("back/assets/js/sidebar-menu.js") }}"></script>

<!--chartist js-->
<script src="{{ asset("back/assets/js/chart/chartist/chartist.js") }}"></script>

<!--chartjs js-->
<script src="{{ asset("back/assets/js/chart/chartjs/chart.min.js") }}"></script>

<!-- lazyload js-->
<script src="{{ asset("back/assets/js/lazysizes.min.js") }}"></script>

<!--copycode js-->
<script src="{{ asset("back/assets/js/prism/prism.min.js") }}"></script>
<script src="{{ asset("back/assets/js/clipboard/clipboard.min.js") }}"></script>
<script src="{{ asset("back/assets/js/custom-card/custom-card.js") }}"></script>

<!--counter js-->
<script src="{{ asset("back/assets/js/counter/jquery.waypoints.min.js") }}"></script>
<script src="{{ asset("back/assets/js/counter/jquery.counterup.min.js") }}"></script>
<script src="{{ asset("back/assets/js/counter/counter-custom.js") }}"></script>

<!--peity chart js-->
<script src="{{ asset("back/assets/js/chart/peity-chart/peity.jquery.js") }}"></script>

<!--sparkline chart js-->
<script src="{{ asset("back/assets/js/chart/sparkline/sparkline.js") }}"></script>

<!--Customizer admin-->
<script src="{{ asset("back/assets/js/admin-customizer.js") }}"></script>
<div class="btn-light custom-theme">RTL</div>

<!--dashboard custom js-->
<script src="{{ asset("back/assets/js/dashboard/default.js") }}"></script>

<!--right sidebar js-->
<script src="{{ asset("back/assets/js/height-equal.js") }}"></script>

<!--height equal js-->
<script src="{{ asset("back/assets/js/height-equal.js") }}"></script>

<!-- lazyload js-->
<script src="{{ asset("back/assets/js/lazysizes.min.js") }}"></script>

<!--script admin-->
<script src="{{ asset("back/assets/js/admin-script.js") }}"></script>