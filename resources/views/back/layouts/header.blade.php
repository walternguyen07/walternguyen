<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Multikart admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Multikart admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ asset("back/assets/images/favicon.png") }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset("back/assets/images/favicon.png") }}" type="image/x-icon">
    <title>Multikart - Premium Admin Template</title>

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset("back/assets/css/fontawesome.css") }}">

    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset("back/assets/css/flag-icon.css") }}">

    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset("back/assets/css/icofont.css") }}">

    <!-- Prism css-->
    <link rel="stylesheet" type="text/css" href="{{ asset("back/assets/css/prism.css") }}">

    <!-- Chartist css -->
    <link rel="stylesheet" type="text/css" href="{{ asset("back/assets/css/chartist.css") }}">

    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset("back/assets/css/bootstrap.css") }}">

    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset("back/assets/css/admin.css") }}">
    @yield('logincss')

    <style type="text/css">
        .jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0) transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }

        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }

    </style>
</head>